package lw.bouncycastle.openpgp.operator;

import lw.bouncycastle.bcpg.PublicKeyPacket;
import lw.bouncycastle.openpgp.PGPException;

public interface KeyFingerPrintCalculator
{
    byte[] calculateFingerprint(PublicKeyPacket publicPk)
        throws PGPException;
}
