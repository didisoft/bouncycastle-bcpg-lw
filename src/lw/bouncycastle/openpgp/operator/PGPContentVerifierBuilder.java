package lw.bouncycastle.openpgp.operator;

import lw.bouncycastle.openpgp.PGPException;
import lw.bouncycastle.openpgp.PGPPublicKey;

public interface PGPContentVerifierBuilder
{
    public PGPContentVerifier build(final PGPPublicKey publicKey)
        throws PGPException;
}
