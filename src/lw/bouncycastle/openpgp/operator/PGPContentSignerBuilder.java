package lw.bouncycastle.openpgp.operator;

import lw.bouncycastle.openpgp.PGPException;
import lw.bouncycastle.openpgp.PGPPrivateKey;

public interface PGPContentSignerBuilder
{
    public PGPContentSigner build(final int signatureType, final PGPPrivateKey privateKey)
        throws PGPException;
}
